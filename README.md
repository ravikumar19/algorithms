# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository for learning algorithms with C++14, python, C# and javascript.
In first phase for this project, I will cover problems defined in a book titled "Introduction of Algorithms"
https://en.wikipedia.org/wiki/Introduction_to_Algorithms

In second phase of this project, I will cover problems defined in a book titled "Dynamic Programming: A Computational Tool"
https://books.google.co.in/books?id=H_m59Mp1kkEC&printsec=frontcover&dq=dynamic+programming+problems+computation&hl=en&sa=X&ved=0ahUKEwjoxJbh4MnTAhUGT7wKHVVBDX8Q6AEIITAA#v=onepage&q=dynamic%20programming%20problems%20computation&f=false

In Thrid and last phase of this project, I will cover problems defined for practices on TopCoder and Code chef
https://community.topcoder.com/tc?module=ProblemArchive
https://www.codechef.com/problems/easy

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Its in progress
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
Its in progress
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Ravi Kumar
wid_ravi@yahoo.co.in
* Repo owner or admin
* Other community or team contact